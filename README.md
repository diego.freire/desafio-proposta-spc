# Desafio para Seleção para Desenvolvedor Java

### Sobre a oportunidade 

A vaga é para Desenvolvedor Java, temos vagas com diversos níveis de senioridade e para cada um deles utilizaremos critérios específicos considerando esse aspecto, combinado? 
Se você for aprovado nesta etapa, será convidado para uma entrevista final com nosso time técnico.

### Introdução ao Desafio Proposta SPC

Olá, queremos convidá-lo a participar de nosso desafio de seleção.  Pronto para participar?

Esse projeto consiste em uma solução para:
  - Captar dados de pessoas 
  - Aprovar/Reprovar proposta de crédito
  - Auditar propostas (aprovando ou reprovando)
  - Transacionar valores (Debitar/Creditar limite) <<<EXTRA>>>.


### Problema

Temos um ecosistema para automatizar e precisamos de sua ajuda!

Precisamos de um sistema para Captar dados de pessoas, disponibilizar crédito através de um banco digital e controlar transações dessa pessoa.

Tudo isso hoje é feito no papel, isso mesmo, ainda estamos no tempos das cavernas.

Mas não se preocupe, já temos algo desenhado para ti dar um norte.

A idéia é a seguinte:

_CRIAR UMA API 1 PARA MANTER PROPOSTAS_
  ```
  * Cadastrando os clientes na plataforma
  * Ao cadastrar, de acordo com a renda da pessoa, disponibilizar um limite para o mesmo. O limite corresponde a 30% da renda da pessoa.
  * É importante que ao cadastrar o cliente na plataforma, o mesmo fique pendente de aprovação, para que outro usuário com permissão de aprovar essas propostas apróve-as.
  ```

_CRIAR OUTRA API 2 QUE SIMULA UM CADASTRO LISTA NEGRA DO SPC/SERASA_
  ```
  * Essa API tem função manter cadastros de pessoas com restrições financeiras
  * Podemos cadastrar um CPF
  * Podemos consultar um CPF
  * Podemos inativar um CPF
  * Podemos consultar um histórico
  ```

_IMPORTANTE:_
  ```
  * QUANDO A API 1 FOR MANDAR ALGUMA PROPOSTA, CONSULTAR A API 2 PARA SABER SE HÁ ALGUMA RESTRICAO.
  ```

_FUNCIONALIDADES EXTRAS DA API 1_
  ```
  * CONSULTAR SE PESSOA ESTÁ NA LISTA NEGRA DA API 2
  * TER OPCAO DE ACORDO PARA RETIRAR PESSOA DA API 2
  * TER OPCAO DE INCLUIR PESSOA NA API 2 
  ```


### tendo em vista as regras citadas acima, segue as informações técnicas para início do trabalho.

## Sugestão de Entidades da API 1:
  ```
  - Pessoa
  - Usuario
  - Endereço
  - Proposta
  ```

## Sugestão de Entidades da API 2:
  ```
  - Pessoa
  - Historico
  ```

## Observação: Você está livre para criar outras entidades de acordo com a necessidade.


### Desafio Técnico
     
  - Pré-requisitos:
    ```
    * Backend em Java
    * Utilização do SGBD Oracle, MySQL ou Postgres.
    * JDK 1.8+
    * Maven 3+ (ou gradle,  ou afins)
    * JUnit 4+
    * Framework Web a critério (Servlets, JSF, Spring MVC, Angular ou afins)
    * Padrões de Projeto
    * Criação de um DUMP com massa de dados.
    ```

  - O que esperamos como escopo:
    ```
    * Adicionar e Manter propostas de crédito;
    * Consultar propostas de crédito, por filtros de dados;
    * Painel de análise de crédito, contendo todos os clientes cadastrados mas que não foram analisados ainda;
    * Perfis diferentes para o usuário que realiza a captação da proposta e o usuário que realiza a análise;
    ```
  - O que não esperamos como escopo:
    ```
    * Fluxo de geração de cartão
    * Qualquer fluxo realizado após a aprovação ou reprovação da proposta
    ```
  
  - O que vamos avaliar:
    ```
    * Seu código; 
    * Organização;
    * Boas práticas;
    ```

### Extras

  - Alguns extras podem ser implentandos como:
    ```
    * Implementar uma rotina de autorização de transação debitando/creditando do limite;
    * Implementar testes unitários; 
    * Utilizar Docker; 
    * Implementar microserviços;
    * Utilizar Kafka ou RabbitMQ ou afins;
    * Inovar com novas tecnologias;
    ```

### Instruções
    ```
    1. Crie um projeto no gitlab;
    2. Desenvolva. Você terá 5 (cinco) dias a partir da data do envio do desafio; 
    3. Após concluir seu trabalho dispnibilize o link do repositório; 
    4. Crie um arquivo de texto com a nomenclatura README.MD com a descrição das funcionalidades implementadas e como devemos fazer o build;
    5. Caso utilize alguma outra tecnologia, exemplo do Docker, incluir no README.MD as instruções para execução;
    6. Envie o link do projeto responsendo ao email enviado.
    ```

### Boa sorte!!!

-----------------------------------------------------------------------------------------
